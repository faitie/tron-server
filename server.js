var app = require("express")(),
	server = require('http').createServer(app),
 	io = require('socket.io').listen(server);
 	
server.listen(80);

//routing
app.get('/',function(req,res){
	res.sendfile(__dirname + '/client.html');
});

var usernames = {};

io.sockets.on('connection', function(socket){
	
	// when client emits 'sendchat' , this listens and executes
	socket.on('sendchat', function(data){
		//we tell the client to execute 'updatechat' with 2 parameters
		io.sockets.emit('updatechat', socket.username, data);
	});
	
	// when client emits 'adduser', this listens and executes
	socket.on('adduser', function(username){
		//we store the username in the socket session of this client
		socket.username = username;
		//add the client's username to the global list
		usernames[username] = username;
		//echo to client they've connected
		socket.emit('updatechat','SERVER','you have connected');
		//echo globaly that a person has connected
		socket.broadcast.emit('updatechat', 'SERVER', username + ' has connected');
		//update the list of users in chat, client side
		io.sockets.emit('updateusers', usernames);
	});
	
	// when the users disconnects perform this
	socket.on('disconnect', function(){
		//remove the username from the global list
		delete usernames[socket.username];
		//update list of users in chat, client side
		io.sockets.emit('updateusers', usernames);
		//echo global that client has left
		socket.broadcast.emit('updatechat', 'SERVER',socket.username / ' has disconnected');
	});
	
});



